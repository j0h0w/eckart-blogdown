---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "1995-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Journal of Social and Evolutionary Systems</em> 18: 397-412"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1995-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Parent/offspring conflict, the extended phenotype and the evolution of the conscience"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
