---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Claudia Engel
date: "2000-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 387-437 in: Mueller, Ulrich; Nauck, Bernhard; Diekmann, Andreas (Hrsg.): <em>Handbuch der Demographie 1: Modelle und Methoden. </em> Berlin (Springer) 2000"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2000-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Menschliche Reproduktion aus verhaltensökologischer Sicht"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
