---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Claudia Engel
date: "1986-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Anthropologischer Anzeiger</em> 44:19-34"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1986-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Ist das postmenopausale Sterbealter Variable einer fitnessmaximierenden Reproduktionsstrategie?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
