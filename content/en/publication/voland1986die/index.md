---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1986-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 31-52 in: Kraus, Otto (Hrsg.) <em>Regulation, Manipulation und Explosion der Bevölkerungsdichte. </em> Göttingen (Vandenhoeck; Ruprecht) 1986"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1986-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Evolution der reproduktiven Selbstbeschränkung"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
