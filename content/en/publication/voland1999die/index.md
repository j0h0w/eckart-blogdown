---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "1999-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 195-209 in: Neumann, Dieter; Schöppe, Arno; Treml, Alfred K. (Hrsg.) <em>Die Natur der Moral - Evolutionäre Ethik und Erziehung</em> Stuttgart (Hirzel) 1999"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1999-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Evolution des Gewissens - oder: Wem nützt das Gute ?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
