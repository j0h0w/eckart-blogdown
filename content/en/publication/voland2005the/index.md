---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Jan Beise
date: "2005-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 239-255 in: Voland, Eckart, Chasiotis, Athanasios; Schiefenhövel, Wulf (eds.): <em>Grandmotherhood - The Evolutionary Significance of the Second Half of Female Life. </em>New Brunswick (Rutgers University Press) 2005"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2005-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "'The Husband's Mother is the Devil in House': Data on the impact of the mother-in-law on stillbirth mortality in historical Krummhörn (1720-1874) and some thoughts on the evolution of postgenerative female life."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
