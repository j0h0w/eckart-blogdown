---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Peter Stephan
date: "2000-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 447-465 in: Van Schaik, Carel P.; Janson, Charles H. (eds.):<em> Infanticide by Males and Its Implications</em> Cambridge (Cambridge University Press) 2000"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2000-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "'The hate that love generated' - Sexually selected neglect of one's own offspring in humans"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
