---
abstract: ""
author_notes: ""
authors: 
- Andreas Paul
- Eckart Voland
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 121-147 in: Keller, Heidi (Hrsg.)<em> Handbuch der Kleinkindforschung 2. Aufl. </em> Bern (Huber) 1997"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die soziobiologische Perspektive - Eltern-Kind-Beziehungen im evolutionären Kontext"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
