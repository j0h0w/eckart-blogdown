---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Caspar Söling
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 35-57 in: Jömann, Norbert; Junker, Christiane; Touma, Chadi (Hrsg.): <em>Religion - wieso, weshalb, warum? Zur Funktion von Religion aus soziologischer, biologischer, philosophischer und theologischer Sicht</em>. Münster (Lit) 2004"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die biologische Basis der Religiosität in den Instinkten - Bausteine einer evolutionären Religionswissenschaft (Nachdruck),"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
