---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2009-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 83-96 in: Kraus, Otto (Hrsg.): <em>Evolutionstheorie und Kreationismus - Ein Gegensatz</em>. Stuttgart (Steiner) 2009"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2009-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Keine menschliche Kultur ohne Religion – Die Gründe,"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
