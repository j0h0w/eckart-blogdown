---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Athanasios Chasiotis
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 220-238 in: Strickland, Simon S.; Shetty, P. S. (eds.) <em>Human Biology and Social Inequality. </em> Cambridge (Cambridge University Press) 1998"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "How female reproductive decisions cause social inequality in male reproductive fitness - Evidence from Eighteenth- and Nineteenth-Century Germany"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
