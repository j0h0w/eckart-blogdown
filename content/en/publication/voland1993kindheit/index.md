---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1993-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 3-15 in: Markefka, Manfred; Nauck, Bernhard (Hrsg.) <em>Handbuch der Kindheitsforschung. </em> Neuwied, Kriftel; Berlin (Luchterhand) 1993"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1993-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Kindheit in evolutionsbiologischer Perspektive"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
