---
abstract: ""
author_notes: ""
authors: 
- Christian Vogel
- Eckart Voland
- Manfred Winter
date: "1979-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 145-181 in: Degenhardt, Annette; Trautner, Hanns Martin (Hrsg.) <em>Geschlechtstypisches Verhalten - Mann und Frau in psychologischer Sicht. </em>München (Beck) 1979"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1979-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Geschlechtstypische Verhaltensentwicklung bei nichtmenschlichen Primaten"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
