---
abstract: ""
author_notes: ""
authors: 
- Amy Parish
- Eckart Voland
date: "2001-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 48-57 in: Haft, Fritjof; Hof, Hagen; Wesche, Steffen (Hrsg):<em> Bausteine zu einer Verhaltenstheorie des Rechts</em> Baden-Baden (Nomos) 2001"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2001-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Cost/benefit considerations in decisions to share in hunter-gatherer societies"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
