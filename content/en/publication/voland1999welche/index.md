---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1999-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 32-48 in: Verein für Ökologie und Umweltforschung (Hrsg.): <em>Werteskala für ökologische Entscheidungen. </em> Wien (Verein für Ökologie und Umweltforschung: Schriftenreihe für Ökologie und Ethologie, Heft 26) 1999"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1999-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Welche Werte? - Ethik, Anthropologie und Naturschutz"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
