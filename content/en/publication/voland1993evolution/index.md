---
abstract: ""
author_notes: ""
authors: 
- Eckart (Hrsg.) Voland
date: "1993-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Stuttgart (Hirzel)"
publication_short: ""
publication_types: 
  - "5"
publishDate: "1993-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolution und Anpassung - Warum die Vergangenheit die Gegenwart erklärt. Christian Vogel zum 60. Geburtstag"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
