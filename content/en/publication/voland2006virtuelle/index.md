---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2006-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Zeitschrift für Literaturwissenschaft und Linguistik </em>37 (146): 7-22"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2006-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Virtuelle Welten in realen Gehirnen - Evolutionspsychologische Aspekte des Umgangs mit Medien"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
