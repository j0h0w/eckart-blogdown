---
abstract: ""
author_notes: ""
authors: 
- Matthias Uhl
- Eckart Voland
date: "2009-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Seoul (Korean Edition)"
publication_short: ""
publication_types: 
  - "5"
publishDate: "2009-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "쇼 오프는 삶에서 더 많은 것을 얻습니다 (Angeber haben mehr vom Leben)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
