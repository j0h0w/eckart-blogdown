---
abstract: ""
author_notes: ""
authors: 
- Jan Beise
- Eckart Voland
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Science</em> 98: 317a"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Effect of producing sons on maternal longevity in premodern populations"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
