---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1992-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Veröffentlichungen des Überseemuseums Bremen - Naturwissenschaften</em> 11: 119-135, 1992"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1992-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Evolution des menschlichen Sozialverhaltens"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
