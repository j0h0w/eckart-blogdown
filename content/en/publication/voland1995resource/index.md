---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Robin I. M. Dunbar
date: "1995-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Human Nature </em>6: 33-49"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1995-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Resource competition and reproduction - The relationship of economic and parental strategies in the Krummhörn population (1720-1874)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
