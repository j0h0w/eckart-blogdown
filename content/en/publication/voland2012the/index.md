---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2012-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Anthropological Review 77:</em> 251-271"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2012-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The biological evolution of conscience – From parent-offspring conflict to morality."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
