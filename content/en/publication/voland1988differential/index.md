---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1988-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 253-261 in: Betzig, Laura L.; Borgerhoff Mulder, Monique; Turke, Paul W. (eds.) <em>Human Reproductive Behaviour - A Darwinian Perspective. </em> Cambridge (Cambridge University Press) 1988"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1988-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Differential infant and child mortality in evolutionary perspective Data from late 17th to 19th century Ostfriesland (Germany)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
