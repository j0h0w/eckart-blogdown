---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1996-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 1116-1123 in: Hubig, Christoph; Poser, Hans (Hrsg.) <em>Cognitio humana - Dynamik des Wissens und der Werte XVII. Deutscher Kongreß für Philosophie</em>, Leipzig 1996, workshop-Beiträge, Band 2, Leipzig (Institut für Philosophie), 1996"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1996-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Moral durch Manipulation ? - Ein evolutionäres Szenario"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
