---
abstract: ""
author_notes: ""
authors: 
- Andreas Thiel
- Eckart Voland
date: "1993-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Der Nervenarz</em>t 64: 623-624"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1993-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Eine angeborene Tötungshemmung beim Menschen gibt es nicht"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
