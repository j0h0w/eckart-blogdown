---
abstract: ""
author_notes: ""
authors: 
- Ruben C. Arslan
- Kai P. Willführ
- Emma M. Frans
- Karin J. H. Verweij
- Paul-Christian Bürkner
- Mikko Myrskylä
- Eckart Voland
- Catarina Almquist
- Brendan P. Zietsch
- Lars Penke
date: "2013-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Proceedings of the Royal Society B</em> 284: 20171562."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2013-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Older fathers' children have lower evolutionary fitness across four centuries and in four populations"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
