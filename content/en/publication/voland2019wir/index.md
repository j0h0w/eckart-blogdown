---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2019-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 5-12 in: Werding, Martin (Hrsg.): <em>Zuwanderung und Integration: Wie reagiert die Aufnahmegesellschaft? Beiträge des Syposiums vom 23. Februar 2018 in der Akademie der Wissenschaften und der Literatur, Mainz</em>. Stuttgart (Steiner) 2019"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2019-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Wir und die anderen - Evolutionäre Angepasstheiten im Umgang mit Fremden."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
