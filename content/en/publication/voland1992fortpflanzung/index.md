---
abstract: ""
author_notes: ""
authors: 
- Eckart (Hrsg.) Voland
date: "1992-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Frankfurt/M (Suhrkamp) 1992(= stw"
publication_short: ""
publication_types: 
  - "5"
publishDate: "1992-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Fortpflanzung: Natur und Kultur im Wechselspiel - Versuch eines Dialogs zwischen Biologen und Sozialwissenschaftlern"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
