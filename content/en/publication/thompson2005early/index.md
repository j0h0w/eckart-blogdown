---
abstract: ""
author_notes: ""
authors: 
- R. A. (Rapporteur) Thompson
- K. Braun
- K. E. Grossmann
- M. R. Gunnar
- M. Heinrichs
- H. Keller
- T. G. O'Connor
- G. Spangler
- E. Voland
- S. Wang
date: "2005-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Thompson, R. A. (Rapporteur); Braun, K.; Grossmann, K. E.; Gunnar, M. R.; Heinrichs, M.; Keller, H.; O'Connor, T. G.; Spangler, G.; Voland, E.; Wang, S. <br /> 
Early social attachment and its consequences. The dynamics of a developing relationship<br /> 
pp. 349-383 in: Carter, C. S.; Ahnert, L.; Grossmann, K. E.; Hrdy, S. B.; Lamb, M. E.; Porges, S. W.; Sachser, N. (eds.):<em> Attachment and Bonding - A New Synthesis</em>.
Boston (MIT Press) - 92nd Dahlem Workshop Report 2005"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2005-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Early social attachment and its consequences. The dynamics of a developing relationship"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
