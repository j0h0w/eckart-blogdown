---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2005-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 35-60 in: Schnell, Ralf (Hrsg.): <em>Wahrnehmung, Kognition, Ästhetik - Neurobiologie und Medienwissenschaften</em>. Bielefeld (transcript) 2005"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2005-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Das 'Handicap-Prinzip' und die biologische Evolution der ästhetischen Urteilskraft"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
