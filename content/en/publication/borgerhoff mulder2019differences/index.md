---
abstract: ""
author_notes: ""
authors: 
- Monique Borgerhoff Mulder
- Mary C. Towner
- Ryan Baldini
- Bret A. Beheim
- Sam Bowles
- Heidi Colleran
- Michael Gurven
- Karen L. Kramer
- M.  Siobhán Mattison
- David A. Nolin
- Brooke A. Scelza
- Rebecca Sear
- Mary K. Shenk
- Eckart Voland
- John Ziker
date: "2019-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Philosophical Transactions of the Royal Society B</em> 374: 20180076"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2019-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Differences between sons and daughters in the intergenerational transmission of wealth"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
