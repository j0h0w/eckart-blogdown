---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 12-26 in: Lüke, Ulrich; Meisinger, Hubert; Souvignier, Georg (Hrsg.): <em>Der Mensch - nichts als Natur?</em> Darmstadt (Wissenschaftliche Buchgesellschaft) 2007"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Natur der Moral - Genese und Geltung in der Ethik"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
