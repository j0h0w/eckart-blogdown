---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Christoph Meißelbach
date: "2021-03-15T00:01:01Z"
doi: "10.1007/s11577-021-00730-6"
featured: false
image: 
  caption: 'Image credit: [**Publisher (Springer)**](https://media.springernature.com/w92/springer-static/cover/journal/11577.jpg)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>KZfSS Kölner Zeitschrift für Soziologie und Sozialpsychologie</em> (2021)."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2021-03-15T00:01:01Z"
slides: []
summary: []
tags: []
title: "Verständigungsbarrieren zwischen kulturalistischen Sozialwissenschaften und evolutionärer Anthropologie: Ursachen, Argumente und Auswege [Barriers of Understanding Between Culturalist Social Sciences and Evolutionary Anthropology: Causes, Arguments, and Ways Forward]"
url_code: []
url_dataset: []
url_pdf: [https://link.springer.com/content/pdf/10.1007/s11577-021-00730-6.pdf]
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
