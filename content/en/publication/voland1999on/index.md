---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1999-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 157-172 in: Bayerts, Kurt (ed.) <em>Solidarity</em> Lancaster (Kluwer) 1999"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1999-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "On the nature of solidarity"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
