---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2013-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Heidelberg & Berlin (Springer Spektrum)"
publication_short: ""
publication_types: 
  - "5"
publishDate: "2013-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Soziobiologie: Die Evolution von Kooperation und Konkurrenz, 4. überarbeitete Auflage"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
