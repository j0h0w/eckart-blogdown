---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Robin I. M. Dunbar
- Claudia Engel
- Peter Stephan
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Current Anthropology</em> 38: 129-135"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Population increase and sex-biased parental investment in humans: Evidence from 18th and 19th century Germany"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
