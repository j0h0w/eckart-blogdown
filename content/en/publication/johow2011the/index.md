---
abstract: ""
author_notes: ""
authors: 
- Johannes Johow
- Molly Fox
- Leslie A. Knapp
- Eckart Voland
date: "2011-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Evolution and Human Behavior 32: 315-325"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2011-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The presence of a paternal grandmother lengthens interbirth interval following the birth of a granddaughter in Krummhörn (18th and 19th centuries)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
