---
abstract: ""
author_notes: ""
authors: 
- Athanasios Chasiotis
- Eckart Voland
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 563-595 in: Keller, Heidi (Hrsg.)<em> Lehrbuch der Entwicklungspsychologie. </em> Bern (Huber) 1998"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Geschlechtliche Selektion und Individualentwicklung"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
