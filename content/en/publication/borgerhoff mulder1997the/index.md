---
abstract: ""
author_notes: ""
authors: 
- Monique Borgerhoff Mulder
- Peter J. Richerson
- Nancy W. Thornhill
- Eckart Voland
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 253-282 in: Weingart, Peter; Mitchell, Sandra D.; Richerson, Peter J.; Maasen, Sabine (eds.) <em>Human by Nature - Between Biology and the Social Sciences. </em> Mahwah; London (Erlbaum) 1997"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The place of behavioral eclogical anthropology in evolutionary social science"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
