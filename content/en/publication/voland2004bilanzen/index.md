---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Jan Beise
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Gießener Hochschulblätter </em>37: 13-22, 2004"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Bilanzen des Alters - oder: Was lehren uns ostfriesische Kirchenbücher über die Evolution von Großmüttern?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
