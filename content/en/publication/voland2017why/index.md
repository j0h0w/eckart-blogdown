---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Kai P. Willführ
date: "2017-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Evolution and Human Behavior</em> 38: 125-135."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2017-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Why does paternal death accelerate the transition to first marriage in the C18-C19 Krummhörn population?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
