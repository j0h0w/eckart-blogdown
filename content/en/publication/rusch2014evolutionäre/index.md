---
abstract: ""
author_notes: ""
authors: 
- Hannes Rusch
- Christoph Lütge
- Eckart Voland
date: "2014-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 163-179 in: Maring, Matthias (Hrsg.):<em> Bereichsethiken im interdisziplinären Dialog</em>. Karlsruhe (KIT Scientific Publishing) 2014"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2014-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionäre und experimentelle Ethik: Eine neue Synthese in der Moralphilosophie?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
