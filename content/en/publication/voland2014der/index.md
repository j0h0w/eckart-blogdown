---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2014-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 15-32 in: Schmidt: Wolf Gerhardt (Hrsg.): <em>Die Natur- Kultur-Grenze in Kunst und Wissenschaft. Historische Entwicklung - interdisziplinäre Differenz - aktueller Forschungsstand</em>. Würzburg (Königshausen; Neumann) 2014"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2014-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Der Scheinriese - Auf der Suche nach der Natur/Kultur-Grenze."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
