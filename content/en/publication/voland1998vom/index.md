---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Andreas Paul
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 35-58 in: Wagner, Michael; Schütze, Yvonne (Hrsg.) <em>Verwandtschaft - Sozialwissenschaftliche Beiträge zu einem vernachlässigtem Thema. </em> Stuttgart (Enke) 1998"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Vom 'egoistischen Gen' zur Familiensolidarität - Die soziobiologische Perspektive von Verwandtschaft"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
