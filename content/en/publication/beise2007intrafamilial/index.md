---
abstract: ""
author_notes: ""
authors: 
- Jan Beise
- Eckart Voland
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>American Journal of Human Biology </em>20: 325-336"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Intrafamilial resource competition and mate competition shaped social-group-specific natal dispersal in the 18th and 19th century Krummhörn population"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
