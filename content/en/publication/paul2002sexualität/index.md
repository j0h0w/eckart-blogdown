---
abstract: ""
author_notes: ""
authors: 
- Andreas Paul
- Uta Skamel
- Eckart Voland
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 17-34 in: Baier, Wilhelm Richard; Wuketits, Franz Manfred (Hrsg.): <em>Mann und Frau - Der Mensch als geschlechtliches Wesen</em> Graz (Leykam) 2002"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Sexualität, Geschlecht und Evolution"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
