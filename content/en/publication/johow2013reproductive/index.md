---
abstract: ""
author_notes: ""
authors: 
- Johannes Johow
- Eckart Voland
- Kai P. Willführ
date: "2013-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 243-259 in: Fisher, Maryanne L.; Garcia, Justin R.; Sokol Chang, Rosemarie (eds.):<em> Evolution’s Empress - Darwinian Perspectives on the Nature of Women</em>. Oxford (Oxford University Press) 2013"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2013-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Reproductive strategies in female postgenerative life"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
