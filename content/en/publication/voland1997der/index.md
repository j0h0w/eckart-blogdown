---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 49-63 in: Baier, Wilhelm R. (Hrsg.):<em> Genetik - Einführung und Kontroverse. </em> Graz (Leykam) 1997"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Der entblößte Affe - Inwieweit bestimmen die Gene unser Verhalten ?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
