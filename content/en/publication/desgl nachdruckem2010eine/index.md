---
abstract: ""
author_notes: ""
authors: 
- desgl. Nachdruck</em>
date: "2010-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 75-87 in: Fink, Helmut (Hrsg.): <em>Der neue Humanismus – Wissenschaftliches Menschenbild und säkulare Ethik</em>. Aschaffenburg (Alibri), 2010"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2010-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Eine Naturgeschichte Gottes? – Zur biologischen Evolution der Religiosität"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
