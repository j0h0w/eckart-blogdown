---
abstract: ""
author_notes: ""
authors: 
- Sabine Gabler
- Eckart Voland
date: "1994-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Human Biology</em> 66: 699-713"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1994-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Fitness of Twinning"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
