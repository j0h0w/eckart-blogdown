---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2006-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 271-291 in: Klein, Uta; Mellmann, Katja; Metzger, Steffanie (Hrsg.): <em>Heuristiken der Literaturwissenschaft – Disziplinexterne Perspektiven auf Literatur.</em> Paderborn (mentis) 2006"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2006-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Das 'Handicap-Prinzip' und die biologische Evolution der ästhetischen Urteilskraft (Nachdruck),"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
