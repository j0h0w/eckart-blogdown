---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Claudia Engel
date: "1990-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Ethology</em> 84: 144-154"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1990-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Female choice in humans: A conditional mate selection strategy of the Krummhörn women (Germany, 1720 -1874)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
