---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Jan Beise
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Behavioral Ecology and Sociobiology</em> 52: 435-443"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Opposite effects of maternal and paternal grandmothers on infant survival in historical Krummhörn"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
