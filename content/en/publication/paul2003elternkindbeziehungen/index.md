---
abstract: ""
author_notes: ""
authors: 
- Andreas Paul
- Eckart Voland
date: "2003-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 149-182 in: Keller, Heidi (Hrsg.):<em> Handbuch der Kleinkindforschung</em>. 3. Aufl. Bern etc. (Huber) 2003"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2003-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Eltern-Kind-Beziehungen im evolutionären Kontext aus soziobiologischer Sicht"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
