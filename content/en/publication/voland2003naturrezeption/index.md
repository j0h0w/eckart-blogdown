---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Thomas Weise
date: "2003-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 237-250 in: Liedtke, Max (Hrsg.): <em>Naturrezeption [Matreier Gespräche]</em> Graz (Austria Medien Service) 2003"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2003-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Naturrezeption, Naturschutz, Nachhaltigkeit - Was lehren uns indigene Völker?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
