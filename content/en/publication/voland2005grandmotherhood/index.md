---
abstract: ""
author_notes: ""
authors: 
- Athanasios  Chasiotis  Eckart Voland
- Wulf Schiefenhövel
date: "2005-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 1-7 in: Voland, Eckart, Chasiotis, Athanasios; Schiefenhövel, Wulf (eds.): <em>Grandmotherhood - The Evolutionary Significance of the Second Half of Female Life. </em>New Brunswick (Rutgers University Press) 2005"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2005-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "'Grandmotherhood: A short overview of three fields of research on the evolutionary significance of postgenerative female life."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
