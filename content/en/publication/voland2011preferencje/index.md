---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2011-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Voland, Eckart<br />Preferencje estetyczne w świecie artefaktów – przystosowanie do osadu 'uczciwych sygnalów'? ('Aesthetic preferences in the world of artefacts: 
Adaptation for the evaluation of 'honest signals'?, pp. 239-260 in: Voland, Eckart; Grammer, Karl (eds.): <em>Evolutionary Aesthetics.</em> Heidelberg etc. (Springer) 2003' übersetzt ins Polnische durch Jerzy Luty) <br />pp. 299-323 in: Milkowski, Marcin (ed.): <em>Filozofia biologii</em>. Warszawa (Przeglad Filozoficzno-Literacki) 2011"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2011-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Preferencje estetyczne w świecie artefaktów – przystosowanie do osadu 'uczciwych sygnalów'?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
