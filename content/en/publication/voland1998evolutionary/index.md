---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Annual Review of Anthropology</em> 27: 347-374"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionary ecology of human reproduction"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
