---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2001-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 326-340 in: Görgens, Sigrid; Scheunpflug, Annette; Stojanov, Krassimir (Hrsg.): <em>Universalistische Moral und weltbürgerliche Erziehung. </em> Frankfurt/M (IKO) 2001"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2001-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Ziele, Chancen und Grenzen weltbürgerlicher Erziehung - Kritische Zwischenrufe eines Soziobiologen in eine pädagogische Debatte"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
