---
abstract: ""
author_notes: ""
authors: 
- Johannes Johow
- Eckart Voland
date: "2011-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 133-146 in: Fink, Helmut; Rosenzweig, Rainer (Hrsg.):<em> Mann, Frau, Gehirn – Geschlechterdifferenz und Neurowissenschaft</em>. Paderborn (mentis) 2011"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2011-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Das geteilte Leben – Evolutionäre Gründe der Geschlechterdifferenz"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
