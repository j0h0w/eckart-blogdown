---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1995-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 137-159 in: Dunbar, Robin I. M. (ed.) <em>Human Reproductive Decisions - Biological and Social Perspectives. </em> London (Macmillan) 1995"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1995-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Reproductive decisions viewed from an evolutionarily informed Historical Demography"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
