---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Robin I. M. Dunbar
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Journal of Biosocial Science</em> 29: 355-360"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The impact of migration and social status on female age at marriage in an historical population"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
