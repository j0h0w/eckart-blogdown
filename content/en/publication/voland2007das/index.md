---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Jan Beise
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 29-42 in: Sudhaus, Walter (Hrsg.): <em>Sitzungsberichte der Gesellschaft Naturforschender Freund zu Berlin </em>(N.F.), Band 45. Keltern (Goecke; Evers) 2007"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Das Paradox der zweiten Lebenshälfte (Nachdruck des Titels: 
Bilanzen des Alters - oder: Was lehren uns ostfriesische Kirchenbücher über die Evolution von Großmüttern?),"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
