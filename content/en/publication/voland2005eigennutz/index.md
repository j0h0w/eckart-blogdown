---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2005-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Zeitschrift für internationale Bildungsforschung und Entwicklungspädagogik</em> 26(4): 15-20"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2005-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Eigennutz und Solidarität - Das konstruktive Potenzial biologisch evolvierter Kooperationsstrategien im Globalisierungsprozess"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
