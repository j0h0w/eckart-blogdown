---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2010-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 110-141 in: Fischer, Klaus Peter; Wiegandt, Klaus (Hrsg.): <em>Evolution und Kultur des Menschen.</em> Frankfurt/M (S. Fischer) 2010"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2010-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die biologische Evolution reproduktiver Strategien - Von natürlicher Fruchtbarkeit zum Zölibat"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
