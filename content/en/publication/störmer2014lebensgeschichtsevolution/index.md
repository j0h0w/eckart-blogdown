---
abstract: ""
author_notes: ""
authors: 
- Charlotte Störmer
- Eckart Voland
date: "2014-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 171-201 in: Neukamm, Martin (Hrsg.): <em> Darwin heute - Evolution als Leitbild in den modernen Wissenschaften</em>. Darmstadt (WBG) 2014"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2014-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Lebensgeschichtsevolution - Variation von Lebensstrategien evolutionär erklären."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
