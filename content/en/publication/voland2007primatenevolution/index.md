---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>ZiF – Mitteilungen</em> 2/2010: 9-16"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Primatenevolution, soziale Intelligenz und Freiheitsintuition (leicht gekürzter und veränderter Nachdruck)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
