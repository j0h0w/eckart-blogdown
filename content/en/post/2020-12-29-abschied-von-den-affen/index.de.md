---
title: Abschied von den Affen?
author: admin
date: '2020-12-08'
slug: abschied-von-den-affen
categories: []
tags: []
subtitle: ''
summary: ''
authors: []
lastmod: '2020-12-29T10:32:10+01:00'
featured: no
image:
  caption: 'Image via [Pixabay](https://cdn.pixabay.com/photo/2017/05/17/10/00/gorilla-2320407_960_720.jpg)'
  focal_point: 'smart'
  preview_only: no
projects: []
---

[„Abschied von den Affen? Warum der Mensch kein Tier sein will“](https://www.ardaudiothek.de/funkkolleg-mensch-und-tier/abschied-von-den-affen-warum-der-mensch-kein-tier-sein-will-folge-02/84030568), von  Dagmar Roehrlich. 
Funkkolleg Mensch und Tier, Folge 2, hr-INFO. 
