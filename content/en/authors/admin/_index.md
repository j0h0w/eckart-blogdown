---
bio: My research interests include distributed robotics, mobile computing and programmable
  matter.
education:
  courses:
  - course: Professor for Philosophy of Biology 
    institution: Institut für Philosophie (formerly known as Zentrum für Philosophie und Grundlagen der Wissenschaften) at the Justus-Liebig University at Giessen
    year: 1995-2015
  - course: Senior Research Fellow at the Dept. of Anthropology, 
    institution: University College London. 
    year: 1993/1994
  - course: Postdoctoral lecture qualification (“Habilitation”) for Anthropology with work on “Historical Demography and Sociobiology”
    institution: University of Göttingen
    year: 1992
  - course: Doctorate (Dr. rer. nat.) with a dissertation on the social behavior of primates
    institution: University of Göttingen
    year: 1978
  - course: Biology and Social Sciences 
    institution: University of Göttingen
    year: 1978
email: "kontakt@eckart-voland.de"
highlight_name: false
interests:
- Evolutionary Anthropology (Sociobiology, Behavioral Ecology) 
- Biophilosophy (Evolutionary Ethics, Evolutionary Aesthetics, Evolutionary Religious Studies) 
- Historical Demography
organizations:
- name: Justus-Liebig-University Gießen
  url: https://www.uni-giessen.de/fbz/fb04/institute/philosophie/
role: |
  Prof. em. Dr. rer. nat.  
  (Philosophy of Biology)
social:
- icon: envelope
  icon_pack: fas
  link: /#contact
- icon: researchgate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Eckart_Voland
- icon: google-scholar
  icon_pack: ai
  link: http://scholar.google.de/citations?user=QOp-OK8AAAAJ&hl=en
superuser: true
title: Eckart Voland
---

Born in 1949 in Hann. Münden (Lower Saxony). Studied Biology and Social Sciences at the University of Göttingen. 1978: Doctorate (Dr. rer. nat.) with a dissertation on the social behavior of primates. German Research Foundation Fellow. 1992: Postdoctoral lecture qualification (“Habilitation”, Univ. of Göttingen) for Anthropology with work on “Historical Demography and Sociobiology”. 1993/94: Senior Research Fellow at the Dept. of Anthropology, University College London. 1995-2015: Professor for Philosophy of Biology at the Institut für Philosophie (formerly known as Zentrum für Philosophie und Grundlagen der Wissenschaften) at the Justus-Liebig University at Giessen. Lectureships at ETH Zurich. Research work primarily in the fields of Evolutionary Anthropology (Sociobiology, Behavioral Ecology), Biophilosophy (Evolutionary Ethics, Evolutionary Aesthetics, Evolutionary Religious Studies) and Historical Demography. Prof. Voland’s writings have been translated into Italian, Korean, Polish, Portuguese and Spanish.


<table width="auto"><tbody><tr><th><strong>Member</strong>:</th>
<td align="left"><a href="http://www.akademie-erfurt.de/" target="_blank">Akademie gemeinnütziger Wissenschaften zu Erfurt</a><br /><br /><a href="http://www.jungius-stiftung.de/" target="_blank">Joachim Jungius-Siftung der Wissenschaften, Hamburg</a></td>
</tr><tr><th><strong>Fellow</strong>:       </th>
<td align="left"><a href="http://www.h-w-k.de/" target="_blank">Hanse Institute for Advanced Studies, Delmenhorst</a><br /><br /><a href="http://www.wiko-greifswald.de/" target="_blank">Alfried Krupp Institute for Advanced Studies, Greifswald</a></td>
</tr><tr><th><strong>Member of advisory board</strong>: </th>
<td align="left"><a href="http://www.giordano-bruno-stiftung.de/" target="_blank">Giordano-Bruno-Stiftung</a><br /><br /><a href="https://hans-albert-institut.de/" target="_blank">Hans-Albert-Institut</a></td>
</tr><tr><th><strong>Member of editorial board:<br /></strong></th>
<td align="left"><a href="http://www.akademiai.com/loi/1126">Evolution, Mind and Behavior - Formerly Journal of Evolutionary Psychology (2003-2018)</a><br /><br /><a href="http://link.springer.com/journal/12110">Human Nature – An Interdisciplinary Biosocial Perspective (1995-2010)</a><br /><br /><a href="http://www.klostermann.de/epages/63574303.sf/de_DE/?ObjectPath=/Shops/63574303/Categories/Zeitschriften/Phil-nat" target="_blank">Philosophia naturalis (2003-2013)</a><br /></td></tbody></table>
