---
date: "2018-04-27T00:00:00Z"
external_link: ""
image:
  caption: Photo by Vlad Stawizki on Unsplash
  focal_point: Smart
links:
- icon: researchgate
  icon_pack: fab
  name: Follow
  link: https://www.researchgate.net/profile/Eckart_Voland
slides: []
summary: Does human moral ability had its origin in the cooperative breeding communities, and therefore, in the conflict-laden intimacy of social proximity?
tags:
- Evolution of Morality
title: Who benefits from the good? The biological evolution of human morality
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

At first glance, hardly any aspect of human life appears to contradict Darwinian interpretations of the world as thoroughly as human morality. Whereas pertinent research in recent decades has made it clear that altruistic behavior can be deemed to be an evolutionary adaptation in many naturalistic scenarios, the circumstances are not quite so clear, with regard to the origin of the ability to make moral judgements generated in the conscience. The problem is that the mostly non-consequential judgement of conscience appears to contradict the consequential operation of natural selection. There are well-justified indications that the biological evolution of conscience and thus the typically human moral ability – other than frequently surmised – is not to be understood as an evolutionary reflex offering social orientation to social cooperation and complexity, but had its origin in the cooperative breeding communities, and therefore, in the conflict-laden intimacy of social proximity. If this hypothesis is accurate, the question of who benefits from conscience - namely the owner of the conscience or those who shape it - is definitely not trivial in an evolutionary adaptive sense.

Supported by: [Alfried Krupp Institute for Advanced Studies, Greifswald](http://www.wiko-greifswald.de/)  

## Project publications (as on Octobre 2018)

Voland, Eckart  
Stichwort: Gewissen  
_Naturwissenschaftliche Rundschau 71_ (8/9): 457-458, 2018  
  
Voland, Eckart; Voland, Renate  
Vom Altruismus zur Moral: Die Helfer-Theorie der Gewissensevolution  
_Biologie in unserer Zeit 45_ (6): 380-387, 2015  
  
Voland, Eckart  
Die konditionierte Moral – Vom evolutionären Eltern/Kind-Konflikt zur
Gewissensmoral. Pp. 160-169 in: Lange, Benjamin P. & Schwarz, Sascha (Hrsg.): _Die menschliche Psyche zwischen Natur und Kultur_. Lengerich (Pabst) 2015  
  
Voland, Eckart & Voland, Renate  
_Evolution des Gewissens – Strategien zwischen Egoismus und Gehorsam_.   Stuttgart (Hirzel) 2014  
  
Voland, Eckart  
The biological evolution of conscience – – From parent-offspring conflict to morality  
_Anthropological Review 77_: 251-271, 2014  
