---
date: "2020-12-27T00:00:00Z"
external_link: ""
image:
  caption: Photo by Vlad Stawizki on Unsplash
  focal_point: Smart
links:
- icon: researchgate
  icon_pack: fab
  name: Follow
  link: https://www.researchgate.net/profile/Eckart_Voland
slides: []
summary: The population of East Frisian Krummhörn, the marsh region northwest of the city of Emden, which was reconstituted on the basis of 18th and 19th century parish records and tax lists for research on evolutionary adaptiveness of socio-cultural behavioral variability.
tags:
- Krummhörn
title: "Family Reconstitution of the Krummhörn (Ostfriesland, 1720-1874): Biological adaptation of human reproductive strategies"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

In this project, the primary issue was to check hypotheses on human reproductive behavior guided by sociobiology and behavioral ecology against the population of East Frisian Krummhörn, the marsh region northwest of the city of Emden, which was reconstituted on the basis of 18th and 19th century parish records and tax lists. Ultimately, the issue was to what extent the evolutionary biology concept of adaptation is suitable for explaining socio-cultural behavioral variability. The "Krummhörn Database" served as the empirical foundation of this research. This database contains vital and social statistical information of 120,852 persons (34,708 families) from 33 neighboring parishes in all.

Many years of support by the _German Research Foundation (DFG)_.

The database (German-language) can be accessed at the repository of [Gesis – Leibnitz Institut für Sozialwissenschaften Köln](https://www.gesis.org/).

[Here](pdf/Datenbankbeschreibung-kurz-e.pdf) you find an introduction to the database.

A list of publications derived from this project can be found [here](pdf/KH-LIT.pdf).  
