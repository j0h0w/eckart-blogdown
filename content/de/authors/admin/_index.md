---
bio: Meine Forschungsarbeiten fallen vorrangig in die Gebiete der Evolutionären Anthropologie (Soziobiologie, Verhaltensökologie), Biophilosophie (Evolutionäre Ethik, Evolutionäre Ästhetik, Evolutionäre Religionswissenschaft) und historische Demografie.  
education:
  courses:
  - course: Professor für Philosophie der Biowissenschaften
    institution: Institut für Philosophie (früher Zentrum für Philosophie und Grundlagen der Wissenschaften) der Justus-Liebig-Universität Gießen.
    year: 1995-2015
  - course: Senior Research Fellow at the Dept. of Anthropology, 
    institution: University College London. 
    year: 1993/1994
  - course: Habilitation (Uni Göttingen) für Anthropologie mit Arbeiten über 'Historische Demografie und Soziobiologie'
    institution: Universität Göttingen
    year: 1992
  - course: Promotion zum Dr. rer. nat. mit einer Arbeit zum Sozialverhalten von Primaten. DFG-Stipendiat.
    institution: Universität Göttingen
    year: 1978
  - course: Studium der Biologie und Sozialwissenschaften 
    institution: Universität Göttingen
    year: 1975
email: "kontakt@eckart-voland.de"
highlight_name: false
interests:
- Evolutionäre Anthropologie (Soziobiologie, Verhaltensökologie) 
- Biophilosophie (Evolutionäre Ethik, Evolutionäre Ästhetik, Evolutionäre Religionswissenschaft) 
- Historische Demografie
organizations:
- name: Justus-Liebig-University Gießen
  url: https://www.uni-giessen.de/fbz/fb04/institute/philosophie/
role: |
  Prof. em. Dr. rer. nat.  
  (Biophilosophie)
social:
- icon: envelope
  icon_pack: fas
  link: /#contact
- icon: researchgate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Eckart_Voland
- icon: google-scholar
  icon_pack: ai
  link: http://scholar.google.de/citations?user=QOp-OK8AAAAJ&hl=en
superuser: true
title: Eckart Voland
---

geb. 1949 in Hann. Münden (Niedersachsen). Studium der Biologie und Sozialwissenschaften an der Universität Göttingen. 1978 Promotion zum Dr. rer. nat. mit einer Arbeit zum Sozialverhalten von Primaten. DFG-Stipendiat. 1992 Habilitation (Uni Göttingen) für Anthropologie mit Arbeiten über "Historische Demografie und Soziobiologie". 1993/94 Senior Research Fellow am Dept of Anthropology, University College London. 1995-2015 Professor für Philosophie der Biowissenschaften am Institut für Philosophie (früher: Zentrum für Philosophie und Grundlagen der Wissenschaften) der Justus-Liebig-Universität Gießen. Lehraufträge an der ETH Zürich. Forschungsarbeiten vorrangig auf den Gebieten der Evolutionären Anthropologie (Soziobiologie, Verhaltensökologie), Biophilosophie (Evolutionäre Ethik, Evolutionäre Ästhetik, Evolutionäre Religionswissenschaft) und historischen Demografie. Schriften Prof. Volands wurden ins Italienische, Koreanische, Polnische, Portugiesische und Spanische übersetzt.


<table width="auto"><tbody><tr><th><strong>Mitgledschaften und Beratung</strong>:</th>
<td align="left"><a href="http://www.akademie-erfurt.de/" target="_blank">Akademie gemeinnütziger Wissenschaften zu Erfurt</a><br /><br /><a href="http://www.jungius-stiftung.de/" target="_blank">Joachim Jungius-Siftung der Wissenschaften, Hamburg</a></td>
</tr><tr><th><strong>Fellow</strong>:       </th>
<td align="left"><a href="http://www.h-w-k.de/" target="_blank">Hanse Institute for Advanced Studies, Delmenhorst</a><br /><br /><a href="http://www.wiko-greifswald.de/" target="_blank">Alfried-Krupp-Wissenschaftskolleg, Greifswald</a></td>
</tr><tr><th><strong>Beiratsmitglied</strong>: </th>
<td align="left"><a href="http://www.giordano-bruno-stiftung.de/" target="_blank">Giordano-Bruno-Stiftung</a><br /><br /><a href="https://hans-albert-institut.de/" target="_blank">Hans-Albert-Institut</a></td>
</tr><tr><th><strong>Member of editorial board:<br /></strong></th>
<td align="left"><a href="http://www.akademiai.com/loi/1126">Evolution, Mind and Behavior - Formerly Journal of Evolutionary Psychology (2003-2018)</a><br /><br /><a href="http://link.springer.com/journal/12110">Human Nature – An Interdisciplinary Biosocial Perspective (1995-2010)</a><br /><br /><a href="http://www.klostermann.de/epages/63574303.sf/de_DE/?ObjectPath=/Shops/63574303/Categories/Zeitschriften/Phil-nat" target="_blank">Philosophia naturalis (2003-2013)</a><br /></td></tbody></table>
