---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Matthias Uhl
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 125-140 in: Liedtke, Max (Hrsg.): <em>Luxurierungen - Beschreibung und Analyse aufwändiger Entwicklungen in Natur und Kultur. </em> Graz (Vehling) [= Matreier Gespräche] 2004"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Luxus zwischen Verschwendung und Investition - Die biologische Funktion von Übertreibung in einer Welt der Knappheit,"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
