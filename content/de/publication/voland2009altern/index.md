---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2009-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 23-43 in: Künemund, Harald; Szydlik, Marc (Hrsg.): <em>Generationen - Multidisziplinäre Perspektiven</em>. Wiesbaden (Verlag für Sozialwissenschaften) 2009"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2009-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Altern und Lebenslauf - ein evolutionsbiologischer Aufriss"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
