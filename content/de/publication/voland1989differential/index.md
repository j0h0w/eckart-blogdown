---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1989-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 391-403 in: Standen, Valerie; Foley, Robert A. (eds.)<em> Comparative Socioecology - The Behavioul Ecology of Humans and other Mammals. </em> Oxford (Blackwell) 1989"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1989-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Differential parental investment: Some ideas on the contact area of European social history and evolutionary biology"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
