---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 27-42 in: Eisenburg, Josef (Hrsg.)<em> Die Freiheit des Menschen - Zur Frage von Verantwortung und Schuld</em> Regensburg (Pustet) 1998"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Moralische Autonomie angesichts 'egoistischer Gene'?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
