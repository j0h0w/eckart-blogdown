---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Sabine Gabler
date: "1994-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Naturwissenschaften</em> 81: 224-225"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1994-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Differential twin mortality indicates a correlation between age and parental effort in humans"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
