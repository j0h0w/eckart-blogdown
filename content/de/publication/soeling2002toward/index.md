---
abstract: ""
author_notes: ""
authors: 
- Caspar Soeling
- Eckart Voland
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Neuroendocrinology Letters</em> 23 (Suppl. 4): 98-104"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Toward an evolutionary psychology of religiosity"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
