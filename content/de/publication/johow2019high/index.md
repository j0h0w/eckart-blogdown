---
abstract: ""
author_notes: ""
authors: 
- Johannes Johow
- Kai P. Willführ
- Eckart Voland
date: "2019-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Evolution and Human Behavior</em> 40: 204-213."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2019-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "High consanguinity promotes intergenerational wealth concentration in socioeconomically privileged Krummhörn families of the 18th and 19th centuries"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
