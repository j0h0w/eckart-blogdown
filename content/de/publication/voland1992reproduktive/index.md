---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1992-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 290-305 in: Voland, Eckart (Hrsg.) <em>Fortpflanzung: Natur und Kultur im Wechselspiel - Versuch eines Dialogs zwischen Biologen und Sozialwissenschaftlern. </em>Frankfurt/M (Suhrkamp) 1992"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1992-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Reproduktive Konsequenzen sozialer Strategien. Das Beispiel der Krummhörner Bevölkerung im 18. und 19. Jahrhundert"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
