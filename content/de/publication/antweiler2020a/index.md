---
abstract: ""
author_notes: ""
authors: 
- Christoph Antweiler
- Hannes Rusch
- Eckart Voland
date: "2020-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 27-51 in: Bochmann, Cathleen & Döring, Helge (Hrsg.): <em>Gesellschaftlichen Zusammenhalt gestalten.</em> Springer (Heidelberg) 2020"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2020-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Ein evolutionär-anthropologischer Blick auf soziale Kohäsion."
url_code: []
url_dataset: []
url_pdf: [https://www.springer.com/de/book/9783658283469]
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
