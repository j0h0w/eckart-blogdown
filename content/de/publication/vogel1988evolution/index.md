---
abstract: ""
author_notes: ""
authors: 
- Christian Vogel
- Eckart Voland
date: "1988-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 101-130: in Immelmann, Klaus; Scherer, Klaus R.; Vogel, Christian; Schmook, Peter (Hrsg.) <em>Psychobiologie - Grundlagen des Verhaltens. </em> Stuttgart, New York (G. Fischer); München (Psychologie-Verlags-Union) 1988"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1988-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolution und Kultur"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
