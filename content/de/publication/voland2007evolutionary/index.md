---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 415-432 in: Dunbar, Robin I. M.; Barrett, Louise (eds.) <em>The Oxford Handbook of Evolutionary Psychology</em>. Oxford (Oxford University Press) 2007"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionary psychology meets history: Insights into human nature through family reconstitution studies."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
