---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "1993-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 210-229 in: Voland, Eckart (Hrsg.)<em> Evolution und Anpassung - Warum die Vergangenheit die Gegenwart erklärt. </em>Christian Vogel zum 60. Geburtstag. Stuttgart (Hirzel) 1993"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1993-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Schuld, Scham und Schande - Zur Evolution des Gewissens"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
