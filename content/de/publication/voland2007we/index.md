---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2007-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Evolutionary Psychology</em> 5: 442-452"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2007-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "We recognize ourselves as being similar to others. Implications of the 'Social Brain Hypothesis' for the biological evolution of the intuition of freedom"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
