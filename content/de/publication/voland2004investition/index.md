---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 9-30 in: Treml, Alfred K. (Hrsg.):<em> Das Alte und das Neue - Erziehung und Bildung in evolutionstheoretischer Sicht.</em> Münster (Lit) 2004"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Investition, Manipulation, Delegation - Soziobiologische Hintergründe dreier Merkmale des menschlichen Erziehungsverhaltens,"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
