---
abstract: ""
author_notes: ""
authors: 
- Andreas Paul
- Eckart Voland
date: "1998-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 99-116 in: Kanitscheider, Bernulf (Hrsg.)<em> Liebe, Lust und Leidenschaft - Sexualität im Spiegel der Wisenschaft</em> Stuttgart (Hirzel) 1998"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1998-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Evolution der Zweigeschlechtlichkeit"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
