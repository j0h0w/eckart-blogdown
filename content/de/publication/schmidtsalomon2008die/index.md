---
abstract: ""
author_notes: ""
authors: 
- Michael Schmidt-Salomon
- Eckart Voland
date: "2008-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 97-123 in: Wetz, Fanz Josef (Hrsg.): <em>Kolleg Praktische Philosophie Band 1: Ethik zwischen Kultur- und Naturwissenschaft</em>. Stuttgart (Reclam) 2008"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2008-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Entzauberung des Bösen"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
