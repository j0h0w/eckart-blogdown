---
abstract: ""
author_notes: ""
authors: 
- Harald Euler
- Eckart Voland
date: "2001-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 277-286 in: Peterson, Steven A.; Somit, Albert (eds.): <em>Evolutionary Approaches in the Behavioral Sciences: Toward a better understanding of human nature.</em> (= <em>Research in Biopolitics</em>, Vol. 8) Oxford (Elsevier) 2001"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2001-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The reception of sociobiology in German psychology and anthropology"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
