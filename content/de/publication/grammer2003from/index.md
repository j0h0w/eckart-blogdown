---
abstract: ""
author_notes: ""
authors: 
- Karl Grammer
- Eckart Voland
date: "2003-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 1-5 in: Voland, Eckart; Grammer, Karl (eds.):<em> Evolutionary Aesthetics.</em>. Heidelberg etc. (Springer) 2003"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2003-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "From Darwin's thoughts on the sense of beauty to evolutionary aesthetics"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
