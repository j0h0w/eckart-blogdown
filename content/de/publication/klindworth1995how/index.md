---
abstract: ""
author_notes: ""
authors: 
- Heike Klindworth
- Eckart Voland
date: "1995-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Human Natur</em>e 6: 221-240"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1995-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "How did the Krummhörn male elite achieve above average reproductive success?"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
