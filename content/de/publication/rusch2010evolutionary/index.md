---
abstract: ""
author_notes: ""
authors: 
- Hannes Rusch
- Eckart Voland
date: "2010-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Aisthesis 6:</em> 113-133"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2010-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionary aesthetics: An introduction to key concepts and current issues."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
