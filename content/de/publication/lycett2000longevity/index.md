---
abstract: ""
author_notes: ""
authors: 
- J. E. Lycett
- R. I. M. Dunbar
- E. Voland
date: "2000-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Proceedings of the Royal Society London</em> B 267: 31-35"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2000-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Longevity and the costs of reproduction in a historical human population"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
