---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1984-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Anthropologischer Anzeiger</em> 38: 165-182"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1984-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Hierarchical structure analysis of the behaviour of captive Demidoff-Galagos (Galago d. demidovii Fischer, 1808) with 'Average-Linkage' cluster analysis"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
