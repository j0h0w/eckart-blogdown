---
abstract: ""
author_notes: ""
authors: 
- Anne E. Rasa
- Christian Vogel
- Eckart (eds.) Voland
date: "1989-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "London & New York (Chapman & Hall)"
publication_short: ""
publication_types: 
  - "5"
publishDate: "1989-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The Sociobiology of Sexual and Reproductive Strategies"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
