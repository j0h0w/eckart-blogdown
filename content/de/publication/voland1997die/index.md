---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Voland, Eckart; Voland, Renate<br /> Die Evolution des Gewissens - oder: Wem nützt das Gute? (desgl. Vorabdruck),<br /> pp. 22-27 in: Treml, Alfred K. (Hrsg.) <em>Natur der Moral? Ethische Bildung im Horizont der modernen Evolutionsforschung</em> (= edition ethik kontrovers 5) Frankfurt/M (Diesterweg) 1997"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Evolution des Gewissens - oder: Wem nützt das Gute? (desgl. Vorabdruck),"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
