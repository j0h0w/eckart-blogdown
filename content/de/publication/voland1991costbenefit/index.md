---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Eva Siegelkow
- Claudia Engel
date: "1991-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Ethology and Sociobiology</em> 12: 105-118"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1991-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Cost/benefit oriented parental investment by high status families - The Krummhörn case"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
