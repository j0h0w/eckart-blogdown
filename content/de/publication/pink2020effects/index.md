---
abstract: ""
author_notes: ""
authors: 
- Katharina E. Pink
- Kai P. Willführ
- Eckart Voland
- Paul Puschmann
date: "2020-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Human Nature 31</em> (2): DOI 10.1007/s12110-020-09368-3"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2020-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Effects of individual mortality experience on out-of-wedlock fertility in Eighteenth- and Nineteenth-century Krummhörn, Germany"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
