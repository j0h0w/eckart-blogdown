---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2010-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Deutsche Zeitschrift für Philosophie</em> 55: 739-749"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2010-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Wir erkennen uns als den anderen ähnlich"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
