---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1985-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Primates</em> 18: 883-901"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1985-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Social play behavior of the Common Marmoset (Callithrix jacchus Erxl. 1777) in captivity"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
