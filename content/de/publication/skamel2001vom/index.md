---
abstract: ""
author_notes: ""
authors: 
- Uta Skamel
- Eckart Voland
date: "2001-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 85-102 in: Huinink, Johannes; Strohmeier, Klaus Peter; Wagner, Michael (Hrsg.):<em> Solidarität in Partnerschaft und Familie - Zum Stand familiensoziologischer Theoriebildung. </em> Würzburg (Ergon) 2001"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2001-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Vom 'ewigen Kampf der Geschlechter' zu Solidarität in Partnerschaft und Familie - Eine soziobiologische Annäherung."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
