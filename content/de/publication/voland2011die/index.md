---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Andreas Paul
date: "2011-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 46-78 in: Keller, Heidi (Hrsg.): <em>Handbuch der Kleinkindforschung.</em> 4. Aufl., Bern (Huber) 2011"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2011-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die soziobiologische Perspektive: Eltern-Kind-Beziehungen im evolutionären Kontext"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
