---
abstract: ""
author_notes: ""
authors: 
- Simone Straka-Geiersbach
- Eckart Voland
date: "1988-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Homo </em>39: 171-185"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1988-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Zum Einfluß der Säuglingssterblichkeit auf die eheliche Fruchtbarkeit (Krummhörn, 18. und 19. Jahrhundert)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
