---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1994-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 83-94 in: Haszprunar, Gerhard; Schwager, Raymond (Hrsg.)<em> Evolution - eine Kontroverse. </em> Thaur, Wien, München (Kulturverlag) 1994"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1994-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Hominisation, Homologie und Heuristik - Ein Plädoyer für den Tier/Mensch-Vergleich"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
