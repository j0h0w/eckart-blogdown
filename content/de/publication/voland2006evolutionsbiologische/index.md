---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2006-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 61-72 in: Pawlik, Kurt (Hrsg.): <em>Handbuch Psychologie - Wissenschaft, Anwendung, Berufsfelder. </em>Heidelberg (Springer) 2006"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2006-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionsbiologische Grundlagen und Methoden."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
