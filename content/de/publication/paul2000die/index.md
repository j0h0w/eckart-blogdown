---
abstract: ""
author_notes: ""
authors: 
- Andreas Paul
- Eckart Voland
date: "2000-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 129-173 in: Jansen, Stephan A.; Schleissing, Stephan (Hrsg.): <em>Konkurrenz und Kooperation - Interdisziplinäre Zugänge zur Theorie der Co-opetition. </em>Marburg (Metropolis) 2000"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2000-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die soziobiologische Perspektive - Eltern-Kind-Beziehungen im evolutionären Kontext (desgl. Nachdruck),"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
