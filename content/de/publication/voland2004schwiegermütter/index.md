---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Jan Beise
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Zeitschrift für Sozialpsychologie</em> 35: 171-184"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Schwiegermütter und Totgeburten - Eine evolutionspsychologische Analyse von Kirchenbuchdaten aus der ostfriesischen Krummhörn des 18. und 19. Jhs."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
