---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 275-286 in: Liedtke, Max (Hrsg.):<em> Orientierung in Raum, Erkenntnis, Weltanschauung, Gesellschaft. </em> Graz (austria medien service) 2002"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Die Natur der menschlichen Kultur - Sechs Antworten der Soziobiologie auf fünf Fragen der Kulturethologie"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
