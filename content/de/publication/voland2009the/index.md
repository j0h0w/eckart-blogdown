---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Wulf (eds.) Schiefenhövel
date: "2009-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Heidelberg (Springer)"
publication_short: ""
publication_types: 
  - "5"
publishDate: "2009-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The Biological Evolution of Religious Mind and Behaviour"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
