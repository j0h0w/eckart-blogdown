---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1997-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 111-133 in: Lampe, Ernst-Joachim (Hrsg.)<em> Zur Entwicklung des Rechtbewußtsein. </em>Frankfurt/M (Suhrkamp) 1997"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1997-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Von der Ordnung ohne Recht zum Recht durch Ordnung - Die Entstehung von Rechtsnormen aus evolutionsbiologischer Sicht"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
