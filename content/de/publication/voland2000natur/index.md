---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2000-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 41-53 in: Fröhlich, Siegfried (Hrsg.): <em>Kultur - Ein interdisziplinäres Kolloquium zur Begrifflichkeit</em> Halle/Saale (Landesamt für Archäologie) 2000"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2000-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Natur oder Kultur? Eine wissenschaftliche Jahrhundertdiskussion entspannt sich."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
