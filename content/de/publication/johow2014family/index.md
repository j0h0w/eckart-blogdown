---
abstract: ""
author_notes: ""
authors: 
- Johannes Johow
- Eckart Voland
date: "2014-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 27-49 in: Otto, Hiltrud; Keller, Heidi (eds.):<em> Different Faces of Attchment - Cultural Variations on a Universal Human Need</em>. Cambridge (Cambridge University Press) 2014"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2014-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Family relations among cooperative breeders: Challenges and offerings to attachment theory from evolutionary anthropology."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
