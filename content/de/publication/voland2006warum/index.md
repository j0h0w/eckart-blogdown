---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2006-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 43-62 in: Heller, Hartmut (Hrsg.): <em>Gemessene Zeit – gefühlte Zeit</em>. Wien; Berlin (Lit) 2006"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2006-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Warum altern wir? – Die biologische Evolution der Vergänglichkeit"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
