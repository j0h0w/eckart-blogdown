---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2011-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 193-205 in: Becker, Patrick; Diewald, Ursula (Hrsg.): <em>Zukunftsperspektiven im theologisch-naturwissenschaftlichen Dialog.</em> Göttingen und Oakville (Vandenhoeck; Ruprecht) 2011"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2011-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionäre Ethik: Moral (fast) ohne Metaphysik"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
