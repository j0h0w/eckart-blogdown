---
abstract: ""
author_notes: ""
authors: 
- J. Fox
- K. Willführ
- A. Gagnon
- L. Dillon
- E. Voland
date: "2017-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>The History of the Family 22:</em> 364-423."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2017-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "The consequences of sibling formation on survival and reproductive success across different ecological contexts: a comparison of the historical Krummhörn and Quebec populations."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
