---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "1989-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Ethology and Sociobiology </em>10: 223-240"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1989-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionary biology and psychiatry: The case of Anorexia nervosa"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
