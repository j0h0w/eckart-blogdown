---
abstract: ""
author_notes: ""
authors: 
- Molly Fox
- Rebecca Sear
- Jan Beise
- Gillian Ragsdale
- Eckart Voland
- Leslie A. Knapp
date: "2010-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Proceedings of the Royal Society B</em> 277: 567-573"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2010-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Grandma plays favorites: X-chromosome relatedness and sex specific childhood mortality."
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
