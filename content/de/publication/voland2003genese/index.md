---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2003-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Philosophia naturalis</em> 41: 139-153"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2003-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Genese und Geltung - Das Legitimationsproblem der Evolutionären Ethik und ein Vorschlag zu seiner Überwindung"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
