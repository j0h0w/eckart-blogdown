---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "2004-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 177-189 in: Lütge, Christoph; Vollmer, Gerhard (Hrsg.): <em>Fakten statt Normen? Zur Rolle einzelwissenschaftlicher Argumente in einer naturalistischen Ethik.</em> Baden-Baden (Nomos) 2004"
publication_short: ""
publication_types: 
  - "6"
publishDate: "2004-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Normentreue zwischen Reziprozität und Prestige-Ökonomie - Soziobiologische Interpretationen kostspieliger sozialer Konformität"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
