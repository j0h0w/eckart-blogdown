---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Renate Voland
date: "2002-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 273-286 in: Hirsch, Klaus &amp; Seitz, Klaus (Hrsg.): Zwischen Sicherheitskalkül, Interesse und Moral. Beiträge zur Ethik der Entwicklungspolitik. Frankfurt/M &amp; London (IKO) 2005"
publication_short: ""
publication_types: 
  - "2"
publishDate: "2002-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Erziehung in einer biologisch determinierten Welt - Herausforderung für die Theoriebildung einer evolutionären Pädagogik aus biologischer Perspektive"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
