---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Claudia Engel
date: "1989-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 194-205 in: Rasa, Anne E.; Vogel, Christian; Voland, Eckart (eds.) <em>The Sociobiology of Sexual and Reproductive Strategies. </em> London; New York (Chapman; Hall) 1989"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1989-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Women's reproduction and longevity in a premodern population (Ostfriesland, Germany, 18th century)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
