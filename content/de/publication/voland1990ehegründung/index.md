---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1990-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 47-73 in: Seifert, Gerhard (Hrsg.) <em>Ehestabilisierende Faktoren. </em> Göttingen (Vandenhoeck; Ruprecht) 1990"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1990-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Ehegründung und Ehestabilität aus evolutionsbiologischer Sicht"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
