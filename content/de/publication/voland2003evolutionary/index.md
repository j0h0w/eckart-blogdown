---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
- Karl (eds.) Grammer
date: "2003-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "Heidelberg (Springer) 2"
publication_short: ""
publication_types: 
  - "5"
publishDate: "2003-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Evolutionary Aesthetics"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
