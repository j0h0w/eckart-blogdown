---
abstract: ""
author_notes: ""
authors: 
- T. Caro
- R. Frank
- D. M. Brown
- A. Parish
- D. W. Sellen
- E. Voland
- M. Borgerhoff Mulder
date: "1995-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>International Journal of Primatology</em> 16: 205-220"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1995-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Termination of reproduction in non-human and human female primates"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
