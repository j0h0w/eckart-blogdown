---
abstract: ""
author_notes: ""
authors: 
- Eckart Voland
date: "1990-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Behavioral Ecology and Sociobiology </em>26: 65-72"
publication_short: ""
publication_types: 
  - "2"
publishDate: "1990-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Differential reproductive success within the Krummhörn population (Germany, 18th and 19th centuries)"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
