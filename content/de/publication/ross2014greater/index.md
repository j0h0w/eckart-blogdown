---
abstract: ""
author_notes: ""
authors: 
- Cody T. Ross
- Monique Borgerhoff Mulder
- Sam  Bowles  Seung-Yun. Oh
- [...]
- Eckart Voland
- Kai [...]  authors Willführ
date: "2014-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "<em>Journal of the Royal Society Interface</em> 15: 20180035."
publication_short: ""
publication_types: 
  - "2"
publishDate: "2014-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Greater wealth inequality, less polygyny: Rethinking the polygyny threshold model"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
