---
abstract: ""
author_notes: ""
authors: 
- Uta Skamel
- Eckart Voland
date: "1999-07-01T00:00:00Z"
doi: ""
featured: false
image: 
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false
projects: []
publication: "pp. 57-65 in: Braun, Thorsten; Elstner, Marcus (Hrsg.) <em>Gene und Gesellschaft</em> Heidelberg (Deutsches Krebsforschungszentrum) 1999"
publication_short: ""
publication_types: 
  - "6"
publishDate: "1999-07-01T00:00:00Z"
slides: []
summary: []
tags: []
title: "Ist das menschliche Verhalten durch die Gene determiniert? - Die soziobiologische Perspektive"
url_code: []
url_dataset: []
url_pdf: []
url_poster: []
url_project: []
url_slides: []
url_source: []
url_video: []
---
