---
date: "2020-12-27T00:00:00Z"
external_link: ""
image:
  caption: Photo by Vlad Stawizki on Unsplash
  focal_point: Smart
links:
- icon: researchgate
  icon_pack: fab
  name: Follow
  link: https://www.researchgate.net/profile/Eckart_Voland
slides: []
summary: The population of East Frisian Krummhörn, the marsh region northwest of the city of Emden, which was reconstituted on the basis of 18th and 19th century parish records and tax lists for research on evolutionary adaptiveness of socio-cultural behavioral variability.
tags:
- Krummhörn
title: "Familienrekonstitution der Krummhörn (Ostfriesland, 1720-1874): Die biologische Angepasstheit menschlicher Reproduktionsstrategien"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

In diesem Projekt geht es vorrangig darum, am Beispiel der aus Kirchenbüchern und Steuerlisten des 18. und 19. Jahrhunderts rekonstituierten Bevölkerung der ostfriesischen Krummhörn, der Marschregion nordwestlich von Emden, soziobiologisch und verhaltensökologisch geleitete Hypothesen zum menschlichen Reproduktionsgeschehen zu überprüfen. Letztlich geht es um die Frage, inwieweit das evolutionsbiologische Anpassungskonzept zur Erklärung sozio-kultureller Verhaltensvariabilität taugt. Als empirische Grundlage dieser Forschung dient die 'Datenbank Krummhörn'. Diese enthält vital- und sozialstatistische Angaben von 120.852 Personen (34.708 Familien) aus insgesamt 33 benachbarten Kirchspielen.

Langjährige Unterstützung durch die _Deutsche Forschungsgemeinschaft_ (DFG).

Die Datenbank ist archiviert bei [Gesis – Leibnitz Institut für Sozialwissenschaften Köln](www.gesis.org).

Eine Datenbankbeschreibung finden Sie [hier (PDF)](pdf/Datenbankbeschreibung-kurz-e.pdf).

Ein Verzeichnis der aus diesem Vorhaben hervorgegangenen Veröffentlichungen finden Sie [hier (PDF)](pdf/KH-LIT.pdf).