---
date: "2018-04-27T00:00:00Z"
external_link: ""
image:
  caption: Photo by Vlad Stawizki on Unsplash
  focal_point: Smart
links:
- icon: researchgate
  icon_pack: fab
  name: Follow
  link: https://www.researchgate.net/profile/Eckart_Voland
slides: []
summary: Liegt der Ursprung menschlicher Moralfähigkeit in den kooperativen Fortpflanzungsgemeinschaften und damit in der konflikthaften Intimität des sozialen Nahbereichs?
tags:
- Evolution der menschlichen Moralfähigkeit
title: Wem nützt das Gute? Die biologische Evolution der menschlichen Moralfähigkeit?
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

Kaum ein Aspekt menschlicher Lebenspraxis scheint auf den ersten Blick so grundsätzlich Darwinischen Weltinterpretationen zu widersprechen wie die menschliche Moral. Während die einschlägige Forschung der letzten Jahrzehnte deutlich gemacht hat, dass in vielen lebensnahen Szenarien altruistisches Verhalten als biologisch funktional und evolutionär angepasst gelten kann, sind die Verhältnisse nicht ganz so klar, wenn es um die Entstehung der im Gewissen generierten moralischen Urteilsfähigkeit geht. Das Problem besteht darin, dass das meist non-konsequenzialistisch ausgerichtete Gewissensurteil der konsequenzialistischen Wirkweise der natürlichen Selektion zu widersprechen scheint. Möglicherweise kann dieser Widerspruch einer Auflösung näher gebracht werden, wenn er – informiert durch neuere Erkenntnisse und Theorieofferten zur sozialen Evolution des Menschen - in einen komplexeren Zusammenhang gestellt wird. So gibt es gut begründete Hinweise, dass die biologische Evolution des Gewissens und damit der typisch menschlichen Moralfähigkeit – anders als häufig vermutet – nicht als soziale Orientierung bietender evolutionärer Reflex auf gesellschaftliche Kooperation und Komplexität zu verstehen ist, sondern ihren Ursprung in den kooperativen Fortpflanzungsgemeinschaften und damit in der konflikthaften Intimität des sozialen Nahbereichs nahm. Wenn diese Hypothese zutreffen sollte, stellt sich die keineswegs triviale Frage, wem eigentlich – in einem evolutionären, adaptiven Sinn - das Gewissen nützt: Seinem Inhaber oder denjenigen, die es formen?

Unterstützung durch: [Alfried-Krupp-Wissenschaftskolleg, Greifswald](http://www.wiko-greifswald.de/).


## Verzeichnis der aus diesem Vorhaben hervorgegangenen Veröffentlichungen

Voland, Eckart  
Stichwort: Gewissen  
_Naturwissenschaftliche Rundschau 71_ (8/9): 457-458, 2018  
  
Voland, Eckart; Voland, Renate  
Vom Altruismus zur Moral: Die Helfer-Theorie der Gewissensevolution  
_Biologie in unserer Zeit 45_ (6): 380-387, 2015  
  
Voland, Eckart  
Die konditionierte Moral – Vom evolutionären Eltern/Kind-Konflikt zur
Gewissensmoral. Pp. 160-169 in: Lange, Benjamin P. & Schwarz, Sascha (Hrsg.): _Die menschliche Psyche zwischen Natur und Kultur_. Lengerich (Pabst) 2015  
  
Voland, Eckart & Voland, Renate  
_Evolution des Gewissens – Strategien zwischen Egoismus und Gehorsam_.   Stuttgart (Hirzel) 2014  
  
Voland, Eckart  
The biological evolution of conscience – – From parent-offspring conflict to morality  
_Anthropological Review 77_: 251-271, 2014  
